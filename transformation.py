import csv
import datetime

delimiteur1 = '|'   
delimiteur2 = ';'
fichier_source = 'input.csv'
fichier_dest = 'output.csv'

#Ouverture du fichier entré
fichier = open(fichier_source, 'r', newline='')


#Création du dictionnaire
colonnes_dict={
    'address': 'adresse_titulaire',
    'carrosserie': 'carrosserie',
    'categorie': 'categorie',
    'couleur': 'couleur',
    'cylindree': 'cylindree',
    'date_immat': 'date_immatriculation',
    'denomination': 'denomination_commerciale',
    'energy': 'energie',
    'firstname': 'prenom',
    'immat': 'immatriculation',
    'marque': 'marque',
    'name': 'nom',
    'places': 'places',
    'poids': 'poids',
    'puissance': 'puissance',
    'type_variante_version': 'type',
    'vin': 'vin',
}

colonnes_modif = ['adresse_titulaire','nom','prenom','immatriculation','date_immatriculation','vin','marque','denomination_commerciale','couleur','carrosserie','categorie','cylindree','energie','places','poids','puissance','type','variante','version']

#OUverture fichier sortie
fichier2 = open(fichier_dest, 'w', newline='')

#changement delimiteur et ordre des colonnes
fichier_w = csv.DictWriter(fichier2, fieldnames=colonnes_modif, delimiter=delimiteur2 )

fichier_w.writeheader()

fichier_r = csv.DictReader(fichier, delimiter=delimiteur1)



for ligne in fichier_r :
    ligne1 = {}
    for indiceP in ligne.keys() : 
        ligne1[colonnes_dict[indiceP]] = ligne[indiceP]
        if indiceP == 'type_variante_version':
            tvv_stock = ligne[indiceP].split(', ')
            ligne1['type'] = tvv_stock[0]
            ligne1['variante'] = tvv_stock[1]
            ligne1['version'] = tvv_stock[2]
        if indiceP == 'date_immat':
            ligne1['date_immatriculation'] = datetime.datetime.strptime(ligne[indiceP], '%Y-%m-%d').strftime('%d/%m/%y')
    ligne={}
    for fieldname in colonnes_modif:
        ligne[fieldname] = ligne1[fieldname]
    fichier_w.writerow(ligne)